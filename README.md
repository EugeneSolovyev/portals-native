# Portals native app
![react-native](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/200px-React-icon.svg.png)
## Clone project via SSH:
    $ git clone git@bitbucket.org:EugeneSolovyev/portals-native.git
## Install app
    $ cd portals-native/
    $ npm install // or 'npm i'
## Link all native dependencies
    $ react-native link
## Run application
    $ react-native run-ios // for iOS
    $ react-native run-android // for Android

## Build Android's APK
    $ npm run android:production
After successful build you can find your APK file in path: ```android/app/build/outputs/apk/release```

## Code Quality Tool

Make sure that ESLint plugin is enabled in your IDE.
For apply project's editor configuration make sure that Project Schema is chosen in "Settings" ("Preferences" in OS X) > "Editor" > "Code Style"