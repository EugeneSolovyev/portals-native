import React from 'react';
import { Root } from "native-base";
import { View, StyleSheet } from 'react-native';
import Store from './application/store/Store';
import { Provider } from 'react-redux';
import { AppNavigator } from './application/route/Routes';

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default () => (
  <Provider store={Store}>
    <Root style={styles.container}>
      <AppNavigator />
    </Root>
  </Provider>
);