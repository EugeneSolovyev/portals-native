import moment from 'moment';
import _ from 'lodash';

const MS_IN_DAY = 86400000;
const SCORE_DATE_FORMAT = 'YYYY-MM-DD';

function QuickSortByLastUpdatedDate(array) {
  if (array.length === 0) return [];
  const left = [];
  const right = [];
  const middle = array[0];
  for (let i = 1; i < array.length; i++) {
    if (array[i].dates.lastUpdated < middle.dates.lastUpdated) {
      left[left.length] = array[i];
    } else {
      right[right.length] = array[i];
    }
  }
  return QuickSortByLastUpdatedDate(left).concat(middle, QuickSortByLastUpdatedDate(right));
}

const setProjects = projects => projects.map(project => ({
  ...project,
  dates: {
    createdAt: moment(project.createdAt).unix(),
    lastUpdated: moment(_.get(project.scoring, '[0].createdAt', project.createdAt)).unix(),
    dayStrike: project.scoring ? _getDayStrike(project.scoring) : 0,
  },
}));

const _getDayStrike = (scores) => {
  if (!scores.length) return 0;
  let today = moment().startOf('day');
  let i = 0;
  let dayStrikes = 0;
  let nextScore;
  do {
    nextScore = moment(scores[i].createdAt, SCORE_DATE_FORMAT);
    if (today.diff(nextScore) === MS_IN_DAY) dayStrikes += 1;
    i += 1;
    today = nextScore;
  } while (i < scores.length && today.diff(nextScore) <= MS_IN_DAY);
  return dayStrikes;
};

const filterRecent = (raw) => {
  const sortedProjects = QuickSortByLastUpdatedDate(raw);
  let result = _.takeRight(sortedProjects, 3);
  return result.reverse();
};

const getScheduled = (raw) => {
  const ScheduledProjects = [];
  const Today = moment().isoWeekday();
  _.forEach(raw, (project) => {
    const Schedule = JSON.parse(_.get(project, 'schedule.value', null));
    const isProjectBelongToThisDay = _.find(Schedule, { dayInWeek: Today });
    if (isProjectBelongToThisDay) ScheduledProjects.push(project);
  });
  return ScheduledProjects;
};

const ProjectHelper = {
  setProjects,
  filterRecent,
  getScheduled,
};

export default ProjectHelper;
