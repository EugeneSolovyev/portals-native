const FILTER_OPERATION_TYPES = {
  NUMERIC: 0,
  COMBINED: 1,
};
const ACTION_OPERATION_TYPES = {
  SIMPLE: 0,
  COMBINED: 1,
};

const ENUMS = {
  CLIENT_LOGIN_PLACES: {
    BUILDER: 0,
    VIEWER: 1,
  },
  CLIENT_PLACES: {
    PROJECT_SELECTOR: 0,
    PROJECT_VIEWER: 1,
    PROJECT_ADDER: 2,
    SETTINGS: 10,
  },
  TOASTER_TYPES: {
    INFO: 0,
    ERROR: 1,
  },
  PREDEFINED_SIZES: {
    RIGHT_PANEL_ITEM_SIZE: 100,
  },
  MAP_ORIENTATION: {
    VERTICAL: 0,
    HORIZONTAL: 1,
  },
  MAP_DIRECTION: {
    FORWARD: 0,
    REVERSE: 1,
  },
  OBJECT_TYPES: {
    NONE: 0,
    ITEM_FROM_RIGHT: 'ITEM_FROM_RIGHT',
    ITEM_FROM_MAP: 'ITEM_FROM_MAP',
    FRAME_FROM_ITEM_PREVIEW: 'FRAME_FROM_ITEM_PREVIEW',

  },
  PROJECT_TYPES: {
    PUBLIC: 0,
    PRIVATE: 1,
  },
  PROJECT_STATUS: {
    NEW: 0,
    PENDING: 1,
    COMPLETED: 2,
    ARCHIVED: 3,
  },
  FILE_TYPES: {
    IMAGE: 0,
    JSON: 1,
    LOTTIE: 2,
  },
  ITEM_TYPES: {
    USUAL: 0,
    BACKGROUND: 1,
    STEP: 2,
  },
  FILTER_OPERATION_TYPES,
  FILTER_OPERATIONS: {
    EQUAL: {
      ID: 0,
      LABEL: '=',
      TYPE: FILTER_OPERATION_TYPES.NUMERIC,
    },
    NOT_EQUAL: {
      ID: 1,
      LABEL: '!=',
      TYPE: FILTER_OPERATION_TYPES.NUMERIC,
    },
    LESS: {
      ID: 2,
      LABEL: '<',
      TYPE: FILTER_OPERATION_TYPES.NUMERIC,
    },
    LESS_OR_EQUAL: {
      ID: 3,
      LABEL: '<=',
      TYPE: FILTER_OPERATION_TYPES.NUMERIC,
    },
    MORE: {
      ID: 4,
      LABEL: '>',
      TYPE: FILTER_OPERATION_TYPES.NUMERIC,
    },
    MORE_OR_EQUAL: {
      ID: 5,
      LABEL: '>=',
      TYPE: FILTER_OPERATION_TYPES.NUMERIC,
    },
    OR: {
      ID: 6,
      LABEL: 'OR',
      TYPE: FILTER_OPERATION_TYPES.COMBINED,
    },
    AND: {
      ID: 7,
      LABEL: 'AND',
      TYPE: FILTER_OPERATION_TYPES.COMBINED,
    },
  },
  EVENT_TYPES: {
    SCORE_CHANGED: {
      ID: 0,
      LABEL: 'Score changed',
    },
    ITEM_STATUS_CHANGED: {
      ID: 1,
      LABEL: 'Item status changed',
    },
    CLICK_ON_ITEM: {
      ID: 3,
      LABEL: 'Click on item',
    },
    ATTACHMENT_START: {
      ID: 4,
      LABEL: 'Attachment start',
    },
    ATTACHMENT_END: {
      ID: 5,
      LABEL: 'Attachment end',
    },
    ATTACHMENT_CLICK: {
      ID: 6,
      LABEL: 'Attachment click',
    },
  },
  CONTENT_STATUS: {
    NOT_VIEWED: {
      ID: 0,
      LABEL: 'Not viewed',
    },
    VIEWED: {
      ID: 1,
      LABEL: 'Viewed',
    },
  },
  ACTION_OPERATION_TYPES,
  ACTION_OPERATIONS: {
    STATUS_CHANGE: {
      ID: 0,
      LABEL: 'Change status',
      TYPE: ACTION_OPERATION_TYPES.SIMPLE,
    },
    CONTENT_SHOW: {
      ID: 1,
      LABEL: 'Show content',
      TYPE: ACTION_OPERATION_TYPES.SIMPLE,
    },
    ADD_SCORE: {
      ID: 2,
      LABEL: 'Add score',
      TYPE: ACTION_OPERATION_TYPES.SIMPLE,
    },
    COMBINED_SYNC: {
      ID: 10,
      LABEL: 'Combined sync',
      TYPE: ACTION_OPERATION_TYPES.COMBINED,
    },
    COMBINED_ASYNC: {
      ID: 11,
      LABEL: 'Combined async',
      TYPE: ACTION_OPERATION_TYPES.COMBINED,
    },
  },
  ATTACHMENTS_TYPES: {
    RICH_TEXT: 0,
    IMAGE: 1,
    VIDEO: 2,
    AUDIO: 3,
    TEST: 4,
    RIDDLE: 5,
    CHECKLIST: 6,
    TIMER: 7,
    STOPWATCH: 8,
    BUTTON: 9,
    YOUTUBE_LINK: 10,
  },
  PROJECT_FLAGS: {
    EMPTY: 0,
  },
};

export default ENUMS;
