const AuthenticationConstants = {
  LOGIN: 'USER_LOGIN',
  REGISTER: 'USER_REGISTER',
  LOGOUT: 'LOGOUT',
  LOADING: 'LOADING',
  ERROR: 'ERROR',
  ADD_SCORE: 'ADD_SCORE',
  GET_STYLES: 'GET_STYLES',
};

export {
  AuthenticationConstants
};