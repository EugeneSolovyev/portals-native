import { ProjectActions } from '../constants/Project';
import _ from 'lodash';

export default (state = {}, action) => {
  switch (action.type) {
    case ProjectActions.GET_CURRENT_PROJECT:
      state = action.payload;
      return state;
    case ProjectActions.UPDATE_PROJECT:
      console.log('action.payload', action.payload);
      console.log('state', state);
      let original = _.get(state, 'maps[0].mapItems', []);
      let bla = _.findIndex(original, { id: action.payload.id });
      console.log(bla);
      original[ bla ] = action.payload;
      return { ...state };
    default:
      return state;
  }
};