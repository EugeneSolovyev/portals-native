import _ from 'lodash';
import { ProjectActions } from '../constants/Project';

export default (state = [], action) => {
  switch (action.type) {
    case ProjectActions.GET_ALL:
      state = action.payload;
      return state;
    case ProjectActions.DELETE_PROJECT_BY_ID:
      const StateClone = _.clone(state);
      const RemovableProject = _.findIndex(StateClone, { id: action.payload });
      if (RemovableProject !== -1) StateClone.splice(RemovableProject, 1);
      return state = StateClone;
    case ProjectActions.ADD_NEW_PROJECT:
      return [
        ...state,
        action.payload,
      ];
    default:
      return state;
  }
};
