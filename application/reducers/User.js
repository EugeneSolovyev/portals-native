import _ from 'lodash';
import { AuthenticationConstants } from '../constants/Authentication';

const InitialState = {
  login: null,
  loading: true,
  error: null,
  styles: null,
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case AuthenticationConstants.LOGIN:
      return { ...state, login: action.login };
    case AuthenticationConstants.LOGOUT:
      state = InitialState;
      return state;
    case AuthenticationConstants.LOADING:
      return { ...state, loading: action.isLoading };
    case AuthenticationConstants.ERROR:
      return { ...state, error: action.error };
    case AuthenticationConstants.ADD_SCORE:
      const score = _.get(state, 'login.statistics.score');
      if (_.isNumber(score)) state.login.statistics.score++;
      return { ...state };
    case AuthenticationConstants.GET_STYLES:
      return { ...state, styles: action.styles };
    default:
      return state;
  }
};
