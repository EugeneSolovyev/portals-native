import { combineReducers } from 'redux';

import User from './User';
import Project from './Project';
import RecentProjects from './RecentProjects';
import CurrentProject from './CurrentProject';
import GroupedProjects from './GroupedProjects';
import ScheduledProjects from './ScheduledProjects';
import ProjectByID from './ProjectByID';

export default combineReducers({
  User,
  Project,
  RecentProjects,
  CurrentProject,
  GroupedProjects,
  ScheduledProjects,
  ProjectByID,
  // ... other reducers
});
