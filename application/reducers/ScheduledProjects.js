import { ProjectActions } from '../constants/Project';

export default (state = [], action) => {
  switch (action.type) {
    case ProjectActions.GET_SCHEDULED_PROJECTS:
      state = action.payload;
      return state;
    default:
      return state;
  }
};
