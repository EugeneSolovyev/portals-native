import { ProjectActions } from '../constants/Project';

export default (state = {}, action) => {
  switch (action.type) {
    case ProjectActions.GET_PROJECT_BY_ID:
      state = action.payload;
      return state;
    default:
      return state;
  }
};