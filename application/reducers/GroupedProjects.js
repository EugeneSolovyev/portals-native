import { ProjectActions } from '../constants/Project';

export default (state = {
  byFlags: [],
  byTags: [],
}, action) => {
  switch (action.type) {
    case ProjectActions.GET_GROUPED:
      if (action.payload.value) {
        state.byTags = action.payload.grouped;
      } else {
        state.byFlags = action.payload.grouped;
      }
      return state;
    default:
      return state;
  }
};
