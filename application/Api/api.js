import axios from 'axios';
import { APP_URL } from 'react-native-dotenv';

const _getUrlWithParams = (url, params) => {
  const chunks = [];
  for (const key in params) {
    const chunk = `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
    chunks.push(chunk);
  }
  if (chunks.length) return `${url}?${chunks.join('&')}`;
  return url;
};

const instance = axios.create({
  baseURL: APP_URL,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
  },
});

instance.interceptors.response.use((response) => {
  if (response.status !== 200) {
    return Promise.reject(response.status);
  }
  return response;
}, error => Promise.reject(error));

const _handleResponse = response => response.data;
const _handleError = ({ response }) => Promise.reject(response.data || 'Error');

const API = {
  getMethod: (url, params) => instance.get(_getUrlWithParams(url, params))
    .then(_handleResponse)
    .catch(_handleError),
  postMethod: (url, payload) => instance.post(url, payload)
    .then(_handleResponse)
    .catch(_handleError),
  putMethod: (url, payload) => instance.put(url, payload)
    .then(_handleResponse)
    .catch(_handleError),
  deleteMethod: (url, payload) => instance.delete(url, payload)
    .then(_handleResponse)
    .catch(_handleError),
};

export { API };
