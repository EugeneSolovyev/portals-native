import React from 'react';
import {
  createStackNavigator, createSwitchNavigator, DrawerActions, createDrawerNavigator,
} from 'react-navigation';
import {
  View, Text, StyleSheet, Platform, TouchableOpacity, Image, StatusBar,
} from 'react-native';

import MainComponent from '../components/MainPage/MainContainer';
import ProjectView from '../components/CurrentProjectView/CurrentProjectViewContainer';
import AuthenticationComponent from '../components/Login/AuthenticationContainer';
import Loading from '../components/Loading/LoadingContainer';
import AddProjectScreen from '../components/AddProjectScreen/AddProjectScreenContainer';
import ProjectCreator from '../components/ProjectCreator/ProjectCreatorContainer';

import Drawer from '../components/Drawer/DrawerContainer';

const AppStack = createStackNavigator({
  Home: MainComponent,
  Project: ProjectView,
  NewProject: AddProjectScreen,
  ProjectCreator,
});

const AuthStack = createStackNavigator({ SignIn: AuthenticationComponent });

const Navigation = createSwitchNavigator(
  {
    Loading,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'Loading',
  },
);

const DrawerNavigator = createDrawerNavigator({
  Home: Navigation,
}, {
  initialRouteName: 'Home',
  contentComponent: Drawer,
  drawerWidth: 300,
});

const MenuImage = ({ navigation }) => {
  if (!navigation.state.isDrawerOpen) {
    return (
      <Image
        style={{
          width: 24,
          height: 24,
        }}
        source={require('./img/menu.png')}
      />
    );
  }
  return (
    <Image
      style={{
        width: 24,
        height: 24,
      }}
      source={require('./img/chevron-pointing-to-the-left.png')}
    />
  );
};

export const AppNavigator = createStackNavigator({
  DrawerNavigator: {
    screen: DrawerNavigator,
  },
}, {
  navigationOptions: ({ navigation }) => ({
    headerLeft: () => (
      <TouchableOpacity
        style={{ marginLeft: 15 }}
        onPress={() => {
          navigation.dispatch(DrawerActions.toggleDrawer());
        }}
      >
        <MenuImage style="styles.bar" navigation={navigation} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#34495e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },

  }),
});
