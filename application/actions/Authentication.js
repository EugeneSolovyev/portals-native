import { AsyncStorage } from 'react-native';
import { AuthenticationConstants } from '../constants/Authentication';
import { API } from '../Api/api';

const USER_KEY = 'auth-portals';

const Login = login => ({
  type: AuthenticationConstants.LOGIN,
  login,
});

const Logout = () => ({
  type: AuthenticationConstants.LOGOUT,
});

const Loading = bool => ({
  type: AuthenticationConstants.LOADING,
  isLoading: bool,
});

const Error = error => ({
  type: AuthenticationConstants.ERROR,
  error,
});

const onSignIn = () => AsyncStorage.setItem(USER_KEY, 'true');
const onSignOut = () => (dispatch) => {
  AsyncStorage.removeItem(USER_KEY);
  dispatch(Logout());
};

const GetCurrentUser = () => dispatch => API.getMethod('viewer/user/current')
  .then(response => dispatch(Login(response)))
  .catch(err => dispatch(Error(err.message || 'ERROR')));

const UserLogin = (payload) => {
  const { email, password } = payload;
  return dispatch => API.postMethod('/auth/login', { email, password })
    .then((response) => {
      dispatch(Login(response));
      onSignIn();
      return true;
    })
    .catch(err => {
      if (err['email']) {
        dispatch(Error(err['email'].join(' ') || 'ERROR'));
      }
      if (err['password']) {
        dispatch(Error(err['password'].join(' ') || 'ERROR'));
      }
      return false;
    });
};

const UserLoginFB = payload => dispatch => API.postMethod('/auth/mobile/facebook', payload)
  .then(response => {
    console.log(response);
  })
  .catch(error => {
    console.log(error);
  });

const UserSignUp = (payload) => {
  return dispatch => API.postMethod('/user/create', payload)
    .then((response) => {
      dispatch(Login(response));
      onSignIn();
      return true;
    })
    .catch(err => {
      if (err['email']) {
        dispatch(Error(err['email'].join(' ') || 'ERROR'));
      }
      if (err['password']) {
        dispatch(Error(err['password'].join(' ') || 'ERROR'));
      }
      return false;
    });
};

const isSignedIn = () => () => new Promise((resolve, reject) => {
  AsyncStorage.getItem(USER_KEY)
    .then((res) => {
      if (res !== null) {
        resolve(true);
      } else {
        resolve(false);
      }
    })
    .catch(err => reject(err));
});

const GetBaseContentOnLogin = () => dispatch => API.getMethod('misc/loginImages')
  .then(styles => dispatch({
    type: AuthenticationConstants.GET_STYLES,
    styles,
  }))
  .catch(err => dispatch(Error(err.message || 'ERROR')));

export {
  UserLogin,
  Loading,
  Error,
  USER_KEY,
  isSignedIn,
  onSignIn,
  onSignOut,
  GetCurrentUser,
  GetBaseContentOnLogin,
  UserSignUp,
  UserLoginFB,
};
