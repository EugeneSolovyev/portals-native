import { Toast } from 'native-base';
import { ProjectActions } from '../constants/Project';
import { AuthenticationConstants } from '../constants/Authentication';
import { API } from '../Api/api';
import ProjectHelper from '../helpers/_projects';

const GetAllProjects = () => dispatch => API.postMethod('/viewer/project/filter', {
  sorting: {
    score: {
      createdAt: 'DESC',
    },
  },
  page: 1,
  numberOnPage: 999,
})
  .then((response) => {
    const builtProjects = ProjectHelper.setProjects(response);
    dispatch({
      type: ProjectActions.GET_ALL,
      payload: builtProjects,
    });
    dispatch({
      type: ProjectActions.GET_RECENT_PROJECTS,
      payload: ProjectHelper.filterRecent(builtProjects),
    });
    dispatch({
      type: ProjectActions.GET_SCHEDULED_PROJECTS,
      payload: ProjectHelper.getScheduled(builtProjects),
    });
    return true;
  })
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

const GetGrouped = (value = false) => dispatch => API.postMethod('project/grouped', {
  page: 1,
  numberOnPage: 4,
  conditions: {
    flags: [
      {
        flagId: 0,
        value,
      },
    ],
  },
})
  .then(grouped => dispatch({
    type: ProjectActions.GET_GROUPED,
    payload: {
      value,
      grouped,
    },
  }))
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

const GetProjectBackground = ({ id, ...rest }) => dispatch => API.getMethod(`/viewer/project/${id}`, rest)
  .then(payload => dispatch({
    type: ProjectActions.GET_CURRENT_PROJECT,
    payload,
  }))
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

const GetProjectForCreate = id => dispatch => API.getMethod(`/project/${id}`)
  .then(payload => dispatch({
    type: ProjectActions.GET_PROJECT_BY_ID,
    payload,
  }))
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

const CreateNewProject = ({ id, data }) => dispatch => API.postMethod(`viewer/project/copyFromEditor/${id}`, data)
  .then(payload => dispatch({
    type: ProjectActions.ADD_NEW_PROJECT,
    payload,
  }))
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

const DeleteProject = payload => dispatch => API.deleteMethod(`/viewer/project/${payload}`)
  .then(() => dispatch({
    type: ProjectActions.DELETE_PROJECT_BY_ID,
    payload,
  }))
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

const UpdateStep = (id, status) => dispatch => API.putMethod(`/viewer/item/map/update/${id}`, { status })
  .then(payload => dispatch({
    type: ProjectActions.UPDATE_PROJECT,
    payload,
  }))
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

const UpdateScore = (projectID, ID, score = 1) => dispatch => API.postMethod(`/viewer/project/${projectID}/score`, {
  score,
  eventInfo: {
    itemId: ID,
  },
})
  .then((payload) => {
    dispatch({
      type: AuthenticationConstants.ADD_SCORE,
      payload,
    });
  })
  .catch(error => Toast.show({
    text: JSON.stringify(error),
  }));

export {
  GetAllProjects,
  GetGrouped,
  GetProjectBackground,
  CreateNewProject,
  GetProjectForCreate,
  DeleteProject,
  UpdateStep,
  UpdateScore,
};
