import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    position: 'relative',
  },
  imageBG: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    padding: 10,
    alignSelf: 'center',
  },
  btnText: {
    color: '#fff',
    fontWeight: '600',
    fontSize: 14,
  },
  projectName: {
    backgroundColor: '#34495e',
    padding: 20,
  },
  projectNameText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '600',
  },
});
