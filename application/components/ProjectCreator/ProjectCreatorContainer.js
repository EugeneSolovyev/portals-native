import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CreateNewProject, GetProjectForCreate } from '../../actions/Project';
import ProjectCreator from './ProjectCreatorComponent';

const mapStateToProps = state => ({
  ProjectByID: state.ProjectByID,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  CreateNewProject,
  GetProjectForCreate,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProjectCreator);