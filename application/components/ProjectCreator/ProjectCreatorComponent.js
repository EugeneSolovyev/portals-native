import React from 'react';
import { View, Text, Image, Dimensions, ImageBackground, Modal, Alert, TouchableOpacity } from 'react-native';
import { Input } from 'native-base';
import { Button, Item } from 'native-base';
import { APP_URL } from 'react-native-dotenv';
import _ from 'lodash';
import styles from './ProjectCreatorStyles';
import CreatorForm from './CreatorFormComponent';
import PortalsModal from '../Modal/PortalsModalComponent';

const { width, height } = Dimensions.get('window');

export default class ProjectCreator extends React.Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      id: this.props.navigation.state.params,
      modalVisible: false,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (_.keys(props.ProjectByID).length !== _.keys(state).length
      || props.ProjectByID.id !== state.id) {
      return {
        ...state,
        ...props.ProjectByID,
      }
    }

    return null;
  }

  componentDidMount() {
    this.props.GetProjectForCreate(this.state.id);
  }

  switchModal = (modalVisible = false) => {
    this.setState({ modalVisible, });
  };

  render() {
    const { image, name } = this.state;
    return (
      <View style={styles.container}>
        <PortalsModal
          modalVisible={this.state.modalVisible}
          switchModal={this.switchModal}
        >
          <CreatorForm
            id={this.state.id}
            switchModal={this.switchModal}
            navigation={this.props.navigation}
            createNewProject={this.props.CreateNewProject}
          />
        </PortalsModal>
        <ImageBackground
          source={{ url: `${APP_URL}${image}` }}
          style={{
            width,
            height: width / 1.3,
            ...styles.imageBG,
          }}
        >
          <Button
            rounded
            success
            onPress={() => this.switchModal(true)}
            style={styles.btn}
          >
            <Text style={styles.btnText}>Start Journey</Text>
          </Button>
        </ImageBackground>
        <View style={styles.projectName}>
          <Text style={styles.projectNameText}>{name}</Text>
        </View>
      </View>
    );
  }
}
