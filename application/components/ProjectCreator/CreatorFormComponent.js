import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Item, Input, Button } from 'native-base';

const { width, height } = Dimensions.get('window');

export default class CreatorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
    };
    this.createProject = this.createProject.bind(this);
  }

  createProject() {
    const { name, description } = this.state;
    const { id, switchModal, navigation, createNewProject } = this.props;
    createNewProject({
      id,
      data: { name, description }
    });
    switchModal();
    navigation.navigate('Home');
  };

  render() {
    const { name, description } = this.state;
    return (
      <View style={{
        marginTop: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        height: height / 4,
      }}>
        <Item rounded>
          <Input
            placeholder='Project Name'
            value={name}
            onChangeText={(name) => this.setState({ name })}
          />
        </Item>
        <Item rounded>
          <Input
            placeholder='Project Description'
            value={description}
            onChangeText={(description) => this.setState({ description })}
          />
        </Item>
        <Button bordered full rounded onPress={this.createProject}>
          <Text>Create Project</Text>
        </Button>
      </View>
    );
  }
}