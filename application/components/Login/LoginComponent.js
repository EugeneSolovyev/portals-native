import React from 'react';
import { Dimensions, View } from 'react-native';
import { Card, CardItem, Item, Input, Button, Text, Body } from 'native-base';

const { width, height } = Dimensions.get('window');

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  render() {
    const { email, password } = this.state;
    const { login } = this.props;
    return (
      <View style={{
        height: height / 4,
        justifyContent: 'space-around',
        alignItems: 'center',
      }}>
        <Item rounded>
          <Input
            placeholder='Email'
            value={email}
            onChangeText={(email) => this.setState({ email })}
          />
        </Item>
        <Item rounded>
          <Input
            secureTextEntry
            placeholder='Password'
            value={password}
            onChangeText={(password) => this.setState({ password })}
          />
        </Item>
        <Button
          bordered
          full
          rounded
          onPress={() => login(this.state)}
        >
          <Text>Login</Text>
        </Button>
      </View>
    );
  }
}
