import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserLogin, UserSignUp, GetBaseContentOnLogin, isSignedIn, UserLoginFB } from '../../actions/Authentication';
import AuthenticationComponent from './AuthenticationComponent';

const mapStateToProps = state => ({
  user: state.User,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  isSignedIn,
  UserSignUp,
  UserLogin,
  GetBaseContentOnLogin,
  UserLoginFB,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticationComponent);
