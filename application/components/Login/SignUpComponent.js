import React from 'react';
import { Dimensions, View } from 'react-native';
import { Card, CardItem, Item, Input, Button, Text, Body } from 'native-base';

const { width, height } = Dimensions.get('window');

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      email: '',
      password: '',
      place: 1,
    };
  }

  render() {
    const { login, email, password } = this.state;
    const { signup } = this.props;
    return (
      <View style={{
        height: height / 3,
        justifyContent: 'space-around',
        alignItems: 'center',
      }}>
        <Item rounded>
          <Input
            placeholder='Username'
            value={login}
            onChangeText={(login) => this.setState({ login })}
          />
        </Item>
        <Item rounded>
          <Input
            placeholder='Email'
            value={email}
            onChangeText={(email) => this.setState({ email })}
          />
        </Item>
        <Item rounded>
          <Input
            secureTextEntry
            placeholder='Password'
            value={password}
            onChangeText={(password) => this.setState({ password })}
          />
        </Item>
        <Button
          bordered
          full
          rounded
          onPress={() => signup(this.state)}
        >
          <Text>Sign Up</Text>
        </Button>
      </View>
    );
  }
}
