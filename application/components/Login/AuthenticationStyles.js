import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  contentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentHeader: {
    color: '#fff',
    fontSize: 20,
  },
  roundedBtn: {
    borderColor: '#fff',
    borderWidth: 5,
    padding: 10,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    marginTop: 20,
    marginBottom: 20,
  },
  roundedBtnText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 18,
  },
});
