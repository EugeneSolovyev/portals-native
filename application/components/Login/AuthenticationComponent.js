import React from 'react';
import { StyleSheet, View, AsyncStorage, Dimensions, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Text } from 'native-base';
import AuthStyles from './AuthenticationStyles';
import { APP_URL } from "react-native-dotenv";
import PortalsModal from '../Modal/PortalsModalComponent';
import Login from './LoginComponent';
import SignUp from './SignUpComponent';
import { LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

const { width, height } = Dimensions.get('window');

class AuthenticationComponent extends React.Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      loginModalVisible: false,
      signUpModalVisible: false,
    };
  }

  componentDidMount() {
    this.props.GetBaseContentOnLogin();
    this.props.isSignedIn()
      .then(isLoggedIn => isLoggedIn && navigation.navigate('App'))
  }

  login = async (user) => {
    const { navigation, UserLogin } = this.props;
    let isAuthenticated = await UserLogin(user);
    if (isAuthenticated) {
      return navigation.navigate('App');
    }
    this.setState({ error: true })
  };

  signup = async (user) => {
    const { navigation, UserSignUp } = this.props;
    let isAuthenticated = await UserSignUp(user);
    if (isAuthenticated) {
      return navigation.navigate('App');
    }
    this.setState({ error: true });
  };

  switchModal = () => {
    this.setState({ loginModalVisible: !this.state.loginModalVisible });
  };

  switchSignUpModal = () => {
    this.setState({ signUpModalVisible: !this.state.signUpModalVisible });
  };

  _responseInfoCallback = async (error, result) => {
    const { navigation, UserLoginFB } = this.props;
    try {
      let isAuthenticated = await UserLoginFB(result)
    } catch (error) {

    }
    // if (error) {
    //   console.log('Error fetching data: ' + error.toString());
    // } else {
    //   console.log(result);
    // }
  };

  onLoginFinished = (error, result) => {
    if (error || result.isCancelled) return;
    const infoRequest = new GraphRequest('/me', {
      parameters: {
        fields: {
          string: 'email,name,first_name,last_name'
        }
      }
    }, this._responseInfoCallback);
    new GraphRequestManager().addRequest(infoRequest).start();
  };

  render() {
    const { styles } = this.props.user;
    return (
      <View>
        {styles && <View>
          <FastImage
            style={{ width, height: height / 3, }}
            source={{ uri: `${APP_URL}${styles.file}`, priority: FastImage.priority.high, }}
          />
          <View style={{
            backgroundColor: styles.backgroundColor,
            width,
            height: height / 1.7,
            ...AuthStyles.contentContainer
          }}>
            <Text style={AuthStyles.contentHeader}>{styles.text}</Text>
            <TouchableOpacity style={{
              backgroundColor: styles.mainColor,
              width: width / 2,
              ...AuthStyles.roundedBtn,
            }} onPress={this.switchModal}>
              <Text style={AuthStyles.roundedBtnText}>Get Started</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.switchSignUpModal}>
              <Text style={AuthStyles.roundedBtnText}>I already have account</Text>
            </TouchableOpacity>
            <LoginButton
              readPermissions={["email", "user_friends", "public_profile"]}
              onLoginFinished={this.onLoginFinished}
              onLogoutFinished={() => console.log("logout.")}/>
          </View>
        </View>}
        <PortalsModal
          modalVisible={this.state.loginModalVisible}
          switchModal={this.switchModal}
          title='Log in'
        >
          <Login login={this.login} />
        </PortalsModal>
        <PortalsModal
          modalVisible={this.state.signUpModalVisible}
          switchModal={this.switchSignUpModal}
          title='Sign Up'
        >
          <SignUp signup={this.signup}/>
        </PortalsModal>
      </View>
    );
  }
}

export default AuthenticationComponent;