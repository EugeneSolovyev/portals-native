import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {},
  heading: {
    fontSize: 20,
    textAlign: 'center',
  },
  menuItem: {
    padding: 10,
    borderBottomWidth: 0.5,
    borderColor: '#d6d7da',
  },
});
