import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { onSignOut } from '../../actions/Authentication';
import Drawer from './DrawerComponent';

const mapStateToProps = state => ({
  user: state.User,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onSignOut,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);
