import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { ScrollView, Text, View, StyleSheet } from 'react-native';
import { DrawerActions } from 'react-navigation';
import _ from 'lodash'
import styles from './DrawerStyles';


class DrawerScreen extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  };

  logout = () => {
    this.props.onSignOut();
    this.props.navigation.navigate('Auth');
  };

  renderAuthenticatedNavigation = () => (
    <View>
      <View style={styles.menuItem}>
        <Text onPress={this.navigateToScreen('Home')}>
          Home
        </Text>
      </View>
      <View style={styles.menuItem}>
        <Text onPress={this.navigateToScreen('NewProject')}>
          New Project
        </Text>
      </View>
      <View style={styles.menuItem}>
        <Text onPress={this.logout}>
          Log Out
        </Text>
      </View>
    </View>
  );

  render() {
    const { login } = this.props.user;
    return (
      <View style={styles.container}>
        <Text>Hi, {_.get(login, 'email', 'Anonymous')}</Text>
        <ScrollView>
          {this.renderAuthenticatedNavigation()}
        </ScrollView>
      </View>
    );
  }
}

DrawerScreen.propTypes = {
  navigation: PropTypes.object
};

export default DrawerScreen;