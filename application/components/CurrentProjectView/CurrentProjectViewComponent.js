import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import _ from 'lodash';
import Menu from '../Menu/MenuComponent';
import CurrentProjectMap from './CurrentProjectMap/CurrentProjectMapComponent';
import ENUMS from "../../constants/_ENIMS_";

const { width: w, height: h } = Dimensions.get('window');

class CurrentProjectView extends React.Component {
  static navigationOptions = { header: null };

  state = {
    loading: false,
    project: _.cloneDeep(this.props.project),
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!_.isEqual(nextProps.project, prevState.project)) {
      return { project: nextProps.project, }
    }

    return null;
  }

  fetchCurrentProject = () => {
    const { navigation, GetProjectBackground } = this.props;
    const { id } = _.get(navigation, 'state.params', {});
    GetProjectBackground({ id, w, h });
  };

  makeStep = (stepProps, stepState, callback) => {
    const { project } = this.state;
    const { id, statusMap } = stepProps;
    const { status } = stepState;
    const UpdatedStatus = status + 1;
    let StepsOnly = _.get(project, 'maps[0].mapItems', []).filter(item => item.type === 2);
    StepsOnly = _.sortBy(StepsOnly, step => step.additionalInfo.stepNumber);

    const PressedItemID = _.findIndex(StepsOnly, step => step.id === id);
    const PrevStepStatus = _.get(StepsOnly, `[${PressedItemID - 1}].isDone`);
    const NextStepID = _.get(StepsOnly, `${PressedItemID + 1}.id`);
    if (!PrevStepStatus && PressedItemID !== 0 || status >= statusMap.length - 1) return;
    // this.applyListeners(stepProps.listeners, stepProps.additionalInfo.stepNumber);
    // console.log(stepProps);
    this.props.UpdateStep(id, UpdatedStatus);
    if (NextStepID) this.props.UpdateStep(NextStepID, status);
    this.props.UpdateScore(project.id, id);
    callback();
  };

  applyListeners = (listeners, currentValue) => {
    _.forEach(listeners, (listener, key) => {
      const listenerFilterValue = _.head(listener.eventFilter.value);
      const listenerActionValue = listener.eventAction.value;
      // if (listenerFilterValue !== currentValue || listenerActionValue !== currentValue) return;

      console.log(listener, currentValue)
    })
  };

  componentDidMount() {
    this.fetchCurrentProject();
  }

  render() {
    const score = _.get(this.props.user, 'login.statistics.score', 0);

    return (
      <View>
        <CurrentProjectMap
          makeStep={this.makeStep}
          {...this.state}
        />
        <Menu
          score={score}
          {...this.props}
        />
      </View>
    );
  }
}

export default CurrentProjectView;