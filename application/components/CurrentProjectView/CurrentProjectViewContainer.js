import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { GetProjectBackground, UpdateStep, UpdateScore } from '../../actions/Project';
import CurrentProjectView from './CurrentProjectViewComponent';

const mapStateToProps = state => ({
  user: state.User,
  project: state.CurrentProject,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  GetProjectBackground,
  UpdateStep,
  UpdateScore,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CurrentProjectView);
