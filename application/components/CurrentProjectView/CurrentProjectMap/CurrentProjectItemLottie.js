import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import LottieView from 'lottie-react-native';
import _ from 'lodash';
import PicStyles from './CurrentProjectItemPicStyles';

export default class CurrentProjectItemLottie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loop: !!this.props.step,
    };
  }

  componentDidUpdate() {
    const { frames } = this.props;
    const Idle = {
      start: _.get(frames, 'lottieSettings.idleStart', 0),
      end: _.get(frames, 'lottieSettings.idleEnd', 0),
    };
    this.animation.reset();
    this.animation.play(Idle.start, Idle.end);
  }

  render() {
    const { src, additionalInfo, step, styles, } = this.props;
    const { loop } = this.state;
    console.log(styles);

    return (
      <Fragment>
        <View style={{ ...PicStyles.container, ...styles }}>
          {src && (
            <LottieView
              ref={animation => this.animation = animation}
              source={src}
              style={styles}
              loop={loop}
            />
          )}
          {additionalInfo && (
            <Text style={{
              fontSize: additionalInfo.fontSize,
              ...PicStyles.text,
            }}
            >
              {additionalInfo.stepNumber}
            </Text>
          )}
        </View>
      </Fragment>
    );
  }
}

CurrentProjectItemLottie.propTypes = {
  src: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.array,
  ]),
  additionalInfo: PropTypes.object,
  step: PropTypes.bool,
  styles: PropTypes.object.isRequired,
};
