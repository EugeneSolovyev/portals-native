import React from 'react';
import { ImageBackground, ScrollView, View, Image, Dimensions } from 'react-native';
import { APP_URL } from "react-native-dotenv";
import CurrentProjectStep from './CurrentProjectStepComponent';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window');
const getPercent = num => Math.ceil(num * DeviceWidth);

export default class CurrentProjectScreen extends React.Component {
  scrollToBottom = (contentWidth, contentHeight) => {
    // const lastActiveItem = this.props.mapItems.slice().reverse().find(item => item.isDone);
    // const y = getPercent(_.get(lastActiveItem, `statusMap[${lastActiveItem.status}].position.y`, 0));
    // const projectLength = _.get(this.props, 'backgroundPieces', []).reduce((height, item) => {
    //   return item.height + height;
    // }, 0);
    // console.log(contentHeight);
    this.scrollView.scrollToEnd();
    // {animated: true})
  };

  renderSteps = () => {
    const { mapItems, makeStep } = this.props;
    return mapItems.map((item, key) => (
      <CurrentProjectStep
        makeStep={makeStep}
        key={key}
        {...item}
      />
    ));
  };

  renderBackground = () => {
    const { backgroundPieces, background } = this.props;
    if (backgroundPieces) {
      return backgroundPieces.map((item, key) => (
        <View key={key}>
          <FastImage
            source={{
              uri: `${APP_URL}${item.path}`,
              priority: FastImage.priority.high,
            }}
            style={{
              width: item.width,
              height: item.height,
              zIndex: 0
            }}
          />
        </View>
      ));
    } else {
      return (
        <View>
          <FastImage
            source={{
              uri: `${APP_URL}${background.value}`,
              priority: FastImage.priority.high,
            }}
            style={{
              width: DeviceWidth,
              height: DeviceHeight,
              zIndex: 0
            }}
          />
        </View>
      );
    }
  };

  render() {
    const { settings } = this.props;
    return (
      <View>
        <ScrollView
          horizontal={!!settings.orientation}
          style={{
            marginBottom: 70
          }}
          ref={ref => this.scrollView = ref}
          onContentSizeChange={this.scrollToBottom}
        >
          {this.renderBackground()}
          {this.renderSteps()}
        </ScrollView>
      </View>
    );
  }
}