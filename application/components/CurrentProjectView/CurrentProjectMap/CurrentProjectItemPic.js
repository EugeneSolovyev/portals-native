import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import PicStyles from './CurrentProjectItemPicStyles';

export default class CurrentProjectItemPic extends React.PureComponent {
  render() {
    const { src, styles, additionalInfo } = this.props;

    return (
      <Fragment>
        <View style={{ ...PicStyles.container, ...styles }}>
          <FastImage
            source={{
              uri: src,
              priority: FastImage.priority.high,
            }}
            style={styles}
            resizeMode="contain"
          />
          {additionalInfo && (
            <Text style={{
              fontSize: additionalInfo.fontSize,
              ...PicStyles.text,
            }}
            >
              {additionalInfo.stepNumber}
            </Text>
          )}
        </View>
      </Fragment>
    );
  }
}

CurrentProjectItemPic.propTypes = {
  src: PropTypes.string.isRequired,
  styles: PropTypes.object.isRequired,
  additionalInfo: PropTypes.object,
};
