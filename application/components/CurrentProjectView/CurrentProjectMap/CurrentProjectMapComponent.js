import React from 'react';
import _ from 'lodash';
import CurrentProjectScreen from './CurrentProjectScreenComponent';

export default class CurrentProjectMap extends React.Component {
  render() {
    const { project, makeStep } = this.props;

    return _.get(project, 'maps', []).map((item, key) => (
      <CurrentProjectScreen
        key={key}
        makeStep={makeStep}
        {...item}
      />
    ));
  }
}