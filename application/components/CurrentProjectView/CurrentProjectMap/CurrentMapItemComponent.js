import React, { Fragment } from 'react';
import CurrentProjectItemLottie from './CurrentProjectItemLottie';
import CurrentProjectItemPic from './CurrentProjectItemPic';

export default class CurrentMapItem extends React.PureComponent {
  render() {
    const {
      isLottie,
      lottie,
      statusMap,
      status,
      StepURI,
      ...other
    } = this.props;

    return (
      <Fragment>
        {isLottie
          ? (
            <CurrentProjectItemLottie
              src={lottie}
              frames={statusMap[status]}
              {...other}
            />
          )
          : (
            <CurrentProjectItemPic
              src={StepURI}
              {...other}
            />
          )}
      </Fragment>
    );
  }
}
