import React from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity } from 'react-native';
import { APP_URL } from "react-native-dotenv";
import _ from 'lodash';
import CurrentMapItem from './CurrentMapItemComponent';
import { API } from "../../../Api/api";

const { width } = Dimensions.get('window');
const getPercent = num => Math.ceil(num * width);

export default class CurrentProjectStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      CurrentFrame: this.getFrameByStatus(this.props.status),
      isLottie: false,
      status: this.props.status,
      lottie: null,
      step: this.props.type === 2,
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let isLottie = nextProps.frames[ nextProps.status ].type === 2;
    if (isLottie) {
      return { isLottie };
    }

    return null;
  }

  componentDidMount() {
    const { CurrentFrame } = this.state;
    this.fetchLottie(CurrentFrame.value)
  }

  getFrameByStatus = status => {
    const { statusMap, frames } = this.props;
    return _.find(frames, { id: +_.get(statusMap, `[${status}].currentFrame`) })
  };

  updateStep = () => {
    let { status } = this.state;
    let UpdatedStatus = status + 1;

    this.setState({
      status: UpdatedStatus,
      CurrentFrame: this.getFrameByStatus(UpdatedStatus),
    }, () => this.fetchLottie(this.state.CurrentFrame.value));
  };

  makeStep = () => this.props.makeStep(this.props, this.state, this.updateStep);

  fetchLottie = (src) => {
    if (!this.state.isLottie) return;
    API.getMethod(src)
      .then(lottie => this.setState({ lottie }));
  };

  render() {
    const { statusMap, additionalInfo  } = this.props;
    const { CurrentFrame, isLottie, status, lottie, step } = this.state;
    const StepURI = `${APP_URL}${_.get(CurrentFrame, 'value')}`;
    const currentSize = _.get(statusMap, `[${status}].size`, 0);
    const currentPositioning = {
      x: _.get(statusMap, `[${status}].position.x`, 0),
      y: _.get(statusMap, `[${status}].position.y`, 0),
    };
    const styles = {
      width: getPercent(currentSize),
      height: getPercent(currentSize),
      zIndex: 20
    };

    return (
      <View style={{
        position: 'absolute',
        bottom: getPercent(currentPositioning.y),
        left: getPercent(currentPositioning.x),
      }}>
        {step
          ?
          <TouchableOpacity onPress={this.makeStep}>
            <CurrentMapItem
              isLottie={isLottie}
              lottie={lottie}
              statusMap={statusMap}
              status={status}
              styles={styles}
              StepURI={StepURI}
              additionalInfo={additionalInfo}
            />
          </TouchableOpacity>
          : <CurrentMapItem
            isLottie={isLottie}
            lottie={lottie}
            statusMap={statusMap}
            status={status}
            styles={styles}
            StepURI={StepURI}
            step
          />
        }
      </View>
    );
  }
}
