import React from 'react';
import { View, StyleSheet, TouchableOpacity, Alert, Image, Text, ImageBackground } from 'react-native';
import { NavigationActions } from "react-navigation";
import MenuStyles from './MenuStyles';

class Menu extends React.Component {
  redirectTo = (path) => {
    const navigateAction = NavigationActions.navigate({
      routeName: path
    });
    this.props.navigation.dispatch(navigateAction);
    // Alert.alert(
    //   `${path} in under development`,
    //   'It will be available soon.',
    //   [
    //     {text: 'OK', onPress: () => console.log('OK Pressed')},
    //   ],
    //   {cancelable: false}
    // );
  };

  render() {
    return (
      <View style={MenuStyles.MenuContainer}>
        <TouchableOpacity onPress={() => this.redirectTo('Home')}>
          <Image
            source={require('./img/portal_selected.png')}
            style={MenuStyles.icon}
            resizeMode='contain'
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.redirectTo('book')}>
          <Image
            source={require('./img/log.png')}
            style={MenuStyles.icon}
            resizeMode='contain'
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.redirectTo('treasure')}>
          <ImageBackground
            source={require('./img/level1.png')}
            style={MenuStyles.iconBig}
            resizeMode='contain'
          >
            <Text style={MenuStyles.scoring}>{this.props.score}</Text>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.redirectTo('clock')}>
          <Image
            source={require('./img/ring.png')}
            style={MenuStyles.icon}
            resizeMode='contain'
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.redirectTo('dude')}>
          <Image
            source={require('./img/profile.png')}
            style={MenuStyles.icon}
            resizeMode='contain'
          />
        </TouchableOpacity>
      </View>
    );
  }
}

Menu.defaultProps = {
  score: 0,
};

export default Menu;