import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  MenuContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 70,
    backgroundColor: '#ecf0f1',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderTopWidth: 2,
    borderTopColor: '#e67e22',
    zIndex: 10,
  },
  icon: {
    width: 30,
    height: 30,
  },
  iconBig: {
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scoring: {
    fontWeight: '600',
    textAlign: 'center',
  },
});
