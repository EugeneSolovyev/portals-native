import React from 'react';
import { ActivityIndicator, StatusBar, View, StyleSheet, AsyncStorage } from 'react-native';
import _ from 'lodash';
import styles from './LoadingStyles';

class Loading extends React.Component {
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    this._bootstrapAsync();
  }

  _bootstrapAsync = () => {
    const { navigation, GetCurrentUser, isSignedIn } = this.props;
    Promise.all([
      isSignedIn(),
      GetCurrentUser(),
    ])
      .then(response => {
        let isAuthenticated = _.every(response.map(item => !!item));
        navigation.navigate(isAuthenticated ? 'App' : 'Auth');
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator/>
        <StatusBar barStyle='default'/>
      </View>
    );
  }
}

export default Loading;