import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isSignedIn, GetCurrentUser } from '../../actions/Authentication';
import Loading from './LoadingComponent';

const mapStateToProps = state => ({
  user: state.User,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  isSignedIn,
  GetCurrentUser,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Loading);