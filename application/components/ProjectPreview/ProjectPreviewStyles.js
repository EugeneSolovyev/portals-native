import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  removableContainer: {
    width: 20,
    height: 20,
    backgroundColor: '#2c3e50',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 0,
    zIndex: 10,
  },
  removeIcon: {
    width: 10,
    height: 10,
  },
  container: {
    margin: 10,
  },
  title: {},
  map: {
    position: 'relative',
    marginBottom: 10,
  },
  bg: {
    width: 150,
    height: 100,
    borderRadius: 20,
  },
  scoreView: {
    width: 30,
    height: 30,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
  score: {
    color: '#fff',
  },
  imageWrapper: {
    width: 155,
    height: 105,
  },
  borderImage: {
    overflow: 'hidden',
  },
  projectName: {
    fontSize: 16,
    fontWeight: '600',
    marginTop: 10,
  },
});
