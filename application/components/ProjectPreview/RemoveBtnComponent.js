import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import styles from "./ProjectPreviewStyles";

export default class RemoveBtn extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.callback}
        style={styles.removableContainer}
      >
        <Image
          source={require('./img/delete.png')}
          style={styles.removeIcon}
        />
      </TouchableOpacity>
    );
  }
}