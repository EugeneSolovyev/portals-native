import React from 'react';
import PropTypes from 'prop-types';
import { APP_URL } from 'react-native-dotenv';
import { View, Text, StyleSheet, Platform, TouchableOpacity, Animated, Easing, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from './ProjectPreviewStyles';
import RemoveBtn from './RemoveBtnComponent';

const COLORS = [ '#1abc9c', '#3498db', '#34495e', '#f39c12', '#c0392b', '#8e44ad' ];
const GetBorderRandomValue = () => Math.floor(Math.random() * 50) + 15;
const GetRotateDeg = () => Math.floor(Math.random() * 3) + 1;
const GetRandomColor = () => COLORS[ Math.floor(Math.random() * COLORS.length) ];

export default class ProjectPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      borderTopLeftRadius: GetBorderRandomValue(),
      borderTopRightRadius: GetBorderRandomValue(),
      borderBottomRightRadius: GetBorderRandomValue(),
      borderBottomLeftRadius: GetBorderRandomValue(),
      transform: [ { rotate: `-${GetRotateDeg()}deg` } ],
      removable: false
    };
  }

  renderScoreBlock = Color => {
    const { totalScoring } = this.props;
    return (
      <View style={{ ...styles.scoreView, backgroundColor: Color }}>
        <Text style={styles.score}>{totalScoring}</Text>
      </View>
    );
  };

  changeRemovableState = () => {
    this.setState({ removable: !this.state.removable })
  };

  onLongPress = () => {
    this.changeRemovableState();
    this.props.longPressCallback(this.props.id)
  };

  _renderRemoveBtn = () => (
    <TouchableOpacity
      onPress={this.onLongPress}
      style={styles.removableContainer}
    >
      <Image
        source={require('./img/delete.png')}
        style={styles.removeIcon}
      />
    </TouchableOpacity>
  );

  render() {
    const { name, image, totalScoring, stepsTotal, id, getProgressBarViaPlatform, callback, longPressCallback } = this.props;
    const { removable, ...rest } = this.state;
    const Color = GetRandomColor();

    return (
      <View style={{
        position: 'relative'
      }}>
        {removable && <RemoveBtn callback={this.onLongPress} />}
        <TouchableOpacity
          style={styles.container}
          onPress={() => callback(id)}
          onLongPress={() => longPressCallback && this.changeRemovableState()}
        >
          <View style={styles.map}>
            <View style={{
              backgroundColor: Color,
              transform: [ { rotate: `${GetRotateDeg()}deg` } ],
              ...styles.imageWrapper,
              ...rest,
            }}>
              <View style={{ ...styles.borderImage, ...rest }}>
                <FastImage
                  style={styles.bg}
                  source={{
                    uri: `${APP_URL}${image}`,
                    priority: FastImage.priority.high,
                  }}
                  resizeMode='cover'
                />
              </View>
            </View>
            {!!totalScoring && this.renderScoreBlock(Color)}
          </View>
          {getProgressBarViaPlatform && getProgressBarViaPlatform(Color, totalScoring, stepsTotal)}
          <Text style={styles.projectName}>{name}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

ProjectPreview.propTypes = {
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired
};