import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  ProgressViewIOS,
  ProgressBarAndroid,
  TouchableOpacity,
  Alert,
  Platform
} from 'react-native';
import { APP_URL } from "react-native-dotenv";
import FastImage from 'react-native-fast-image';
import styles from './ProjectCardStyles';

const COLORS = [ '#1abc9c', '#3498db', '#34495e', '#f39c12', '#c0392b', '#8e44ad' ];
const GetBorderRandomValue = () => Math.floor(Math.random() * 250) + 75;
const GetRotateDeg = () => Math.floor(Math.random() * 3) + 1;
const GetRandomColor = () => COLORS[ Math.floor(Math.random() * COLORS.length) ];


export default class ProjectCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      borderTopLeftRadius: GetBorderRandomValue(),
      borderTopRightRadius: GetBorderRandomValue(),
      borderBottomRightRadius: GetBorderRandomValue(),
      borderBottomLeftRadius: GetBorderRandomValue(),
      transform: [ { rotate: `-${GetRotateDeg()}deg` } ],
    };
  }

  render() {
    const { name, image, totalScoring, stepsTotal, id, getProgressBarViaPlatform } = this.props;
    const Color = GetRandomColor();

    return (
      <TouchableOpacity style={styles.container} onPress={() => this.props.callback(id)}>
        <View style={styles.card}>
          <View style={styles.projectInfoBlock}>
            <Text style={styles.projectScore}>{totalScoring}</Text>
            <Text>{name}</Text>
          </View>
          <View style={{
            backgroundColor: Color,
            transform: [ { rotate: `${GetRotateDeg()}deg` } ],
            ...styles.imageWrapper,
            ...this.state,
          }}>
            <View style={{ ...styles.borderImage, ...this.state }}>
              <FastImage
                style={styles.projectImage}
                source={{
                  uri: `${APP_URL}${image}`,
                  priority: FastImage.priority.high,
                }}
              />
            </View>
          </View>
        </View>
        {getProgressBarViaPlatform && getProgressBarViaPlatform(Color, totalScoring, stepsTotal)}
      </TouchableOpacity>
    );
  }
}