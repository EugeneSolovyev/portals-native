import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    margin: 10,
  },
  card: {
    padding: 10,
    height: 130,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: '#fff',
    shadowOpacity: 0.5,
    shadowRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      height: 1,
      width: -1,
    },
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  projectInfoBlock: {
    alignSelf: 'flex-start',
  },
  imageWrapper: {
    width: 155,
    height: 105,
  },
  borderImage: {
    overflow: 'hidden',
    width: 150,
    height: 100,
  },
  projectImage: {
    width: 150,
    height: 100,
  },
  projectScore: {
    fontSize: 40,
    fontWeight: '800',
    alignSelf: 'flex-start',
  },
});
