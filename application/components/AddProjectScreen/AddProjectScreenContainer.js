import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { GetGrouped } from '../../actions/Project';

import AddProjectScreen from './AddProjectScreenComponent';

const mapStateToProps = state => ({
  user: state.User,
  projects: state.GroupedProjects,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  GetGrouped,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddProjectScreen);
