import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Container, Tab, Tabs } from 'native-base';
import Menu from '../Menu/MenuComponent';
import AddProjectMaps from '../AddProjectMaps/AddProjectMapsComponent';
import _ from 'lodash';

class AddProjectScreen extends React.Component {
  static navigationOptions = { header: null };
  
  componentDidMount() {
    this.props.GetGrouped();
    this.props.GetGrouped(true);
  }

  toProjectCreating = (project) => {
    this.props.navigation.navigate('ProjectCreator', project);
  };
  
  render () {
    const score = _.get(this.props.user, 'login.statistics.score', 0);
    return (
      <Container>
        <Tabs>
          <Tab heading='Maps'>
            <AddProjectMaps
              callback={() => console.log('ping')}
              type={'byTags'}
              {...this.props}
            />
          </Tab>
          <Tab heading='Filled Maps'>
            <AddProjectMaps
              callback={this.toProjectCreating}
              type={'byFlags'}
              {...this.props}
            />
          </Tab>
        </Tabs>
        <Menu
          score={score}
          {...this.props}
        />
      </Container>
    );
  };
}

export default AddProjectScreen;