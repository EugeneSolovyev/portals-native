import React from 'react';
import PropTypes from 'prop-types';
import { Modal, View, TouchableOpacity, Image, Dimensions, Text } from 'react-native';
import styles from './PortalsModalStyles';

const { width } = Dimensions.get('window');

export default class PortalsModal extends React.Component {
  render() {
    const { modalVisible, switchModal, children, title } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
      >
        <View style={styles.modalWrapper}>
          <View style={{ width: width / 1.2, ...styles.modalContainer }}>
            <View style={styles.modalBody}>
              {title && <Text style={styles.title}>{title}</Text>}
              <TouchableOpacity onPress={() => switchModal()}>
                <Image
                  source={require('./img/delete.png')}
                  style={styles.closeBtn}
                />
              </TouchableOpacity>
            </View>
            {children}
          </View>
        </View>
      </Modal>
    );
  }
}

PortalsModal.propTypes = {
  modalVisible: PropTypes.bool.isRequired,
  switchModal: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
};
