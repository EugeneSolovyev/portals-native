import React from 'react';
import { Image, ScrollView, View, Dimensions, StyleSheet } from 'react-native';
import { H2 } from 'native-base';
import { APP_URL } from 'react-native-dotenv';
import ProjectPreview from '../ProjectPreview/ProjectPreviewComponent';
import styles from './AddProjectMapsStyles';

class AddProjectMaps extends React.Component {
  render() {
    const { projects, type } = this.props;
    return (
      <View style={styles.Wrapper}>
        <ScrollView>
          {projects[ type ].map((map, key) => (
            <View key={key} style={styles.block}>
              <H2 style={styles.blockName}>{map.name}</H2>
              <ScrollView horizontal>
                {map.projects.map((project, key_project) => (
                  <ProjectPreview
                    callback={this.props.callback}
                    key={key_project}
                    {...project}
                  />
                ))}
              </ScrollView>
            </View>
          ))}
        </ScrollView>
      </View>
    );
  }
}

export default AddProjectMaps;