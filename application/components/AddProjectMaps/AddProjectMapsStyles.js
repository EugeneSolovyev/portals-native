import { Dimensions, StyleSheet } from 'react-native';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

export default StyleSheet.create({
  Wrapper: {
    marginBottom: 70,
  },
  block: {
    borderBottomWidth: 1,
    borderBottomColor: '#ecf0f1',
    padding: 20,
  },
  blockName: {
    fontWeight: '100',
    marginBottom: 10,
  },
  projectContainer: {
    marginRight: 10,
  },
  imageContainer: {
    borderRadius: 20,
    overflow: 'hidden',
    marginBottom: 10,
  },
  image: {
    width: deviceWidth / 2.3,
    height: deviceHeight / 7,
  },
  projectName: {
    fontWeight: '600',
  },
});
