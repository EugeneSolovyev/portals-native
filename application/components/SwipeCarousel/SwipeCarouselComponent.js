import React from 'react';
import { View, Dimensions } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

const { width: viewportWidth } = Dimensions.get('window');

export default class SwipeCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0
    };
  }
  
  renderItem = ({item}) => {
    const { component: SwipeComponent } = this.props;
    return <SwipeComponent
      item={item}
      {...this.props}
    />;
  };
  
  get pagination () {
    const { activeSlide } = this.state;
    const { data } = this.props;
    return (
      <Pagination
        dotsLength={data.length}
        activeDotIndex={activeSlide}
        containerStyle={{
          backgroundColor: 'rgba(0, 0, 0, 0.75)'
        }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 2,
          backgroundColor: 'rgba(255, 255, 255, 0.92)'
        }}
        inactiveDotStyle={{
          // Define styles for inactive dots here
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }
  
  render () {
    const { data } = this.props;
    return (
      <View>
        <Carousel
          data={data}
          renderItem={this.renderItem}
          sliderWidth={viewportWidth}
          itemWidth={viewportWidth}
          enableMomentum
          onSnapToItem={(index) => this.setState({ activeSlide: index }) }
        />
        {this.pagination}
      </View>
    );
  }
}