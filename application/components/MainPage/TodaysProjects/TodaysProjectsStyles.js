import { StyleSheet } from "react-native";

const TextShadow = {
  textShadowColor: 'rgba(0, 0, 0, 0.75)',
  textShadowOffset: { width: -1, height: 1 },
  textShadowRadius: 2,
};

export default StyleSheet.create({
  containerWrapper: {
    width: '100%',
    height: 400,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  explanation: {
    color: '#fff',
    textTransform: 'uppercase',
    fontWeight: '800',
    fontSize: 10,
    ...TextShadow
  },
  title: {
    color: '#fff',
    fontSize: 30,
    fontWeight: '800',
    textAlign: 'center',
    ...TextShadow
  },
  button: {
    width: '40%',
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 25,
    borderWidth: 3,
    borderColor: '#fff',
    marginTop: 20
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  strikeBlock: {
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  strikeText: {
    color: '#fff',
    fontSize: 12,
    ...TextShadow
  }
});