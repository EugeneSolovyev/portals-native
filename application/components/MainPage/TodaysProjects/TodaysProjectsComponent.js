import React from 'react';
import { View, StyleSheet, Text, ImageBackground, TouchableHighlight, Image } from 'react-native';
import { APP_URL } from "react-native-dotenv";
import styles from './TodaysProjectsStyles';

export default class TodaysProjects extends React.Component {
  constructor() {
    super();
  }

  _startProject = () => {
    this.props.navigation.navigate('Project', {
      id: this.props.item.id
    });
  };

  render() {
    const { item: { name, image, dates } } = this.props;

    return (
      <ImageBackground
        source={{ uri: `${APP_URL}${image}` }}
        style={styles.containerWrapper}
      >
        <View
          style={styles.container}
        >
          <View style={styles.strikeBlock}>
            <Image
              source={require('./img/diamond.png')}
              style={{ width: 30, height: 30 }}
            />
            <Text style={styles.strikeText}>{dates.dayStrike} days</Text>
          </View>
          <Text
            style={styles.explanation}>
            Next for today
          </Text>
          <Text
            style={styles.title}
          >
            {name}
          </Text>
          <TouchableHighlight
            style={styles.button}
            onPress={this._startProject}
          >
            <Text
              style={styles.buttonText}
            >
              START
            </Text>
          </TouchableHighlight>
        </View>
      </ImageBackground>
    );
  }
}