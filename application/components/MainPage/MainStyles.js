import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  MainScreenWrap: {
    flex: 1,
  },
  MainScreen: {
    marginBottom: 70,
  },
  loadingBackgroundStyle: {
    backgroundColor: 'rgba(125, 125, 255, 1)',
  },
  progressBar: {
    transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }],
    marginTop: -10,
  },
});
