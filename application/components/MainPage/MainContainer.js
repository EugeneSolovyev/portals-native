import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { GetAllProjects, DeleteProject } from "../../actions/Project";
import { onSignOut } from '../../actions/Authentication';
import MainComponent from './MainComponent'

const mapStateToProps = state => ({
  user: state.User,
  projects: state.Project,
  recentProjects: state.RecentProjects,
  scheduled: state.ScheduledProjects,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  GetAllProjects,
  DeleteProject,
  onSignOut,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);