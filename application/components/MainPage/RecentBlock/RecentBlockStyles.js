import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  Header: {
    fontSize: 20,
    fontWeight: '100',
    margin: 10,
  },
});
