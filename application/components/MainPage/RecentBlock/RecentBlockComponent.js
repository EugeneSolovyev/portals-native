import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ProjectCard from '../../ProjectCard/ProjectCardComponent';
import styles from './RecentBlockStyles';

export default class RecentBlock extends React.PureComponent {
  render() {
    const { projects } = this.props;
    return (
      <View>
        <Text style={styles.Header}>Resent</Text>
        {projects.map((project, key) => (
          <ProjectCard
            key={key}
            getProgressBarViaPlatform={this.props.getProgressBarViaPlatform}
            callback={this.props.callback}
            {...project}
          />
        ))}
      </View>
    );
  }
}