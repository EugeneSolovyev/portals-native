import React from 'react';
import { View, ScrollView, StyleSheet, Platform, ProgressViewIOS, ProgressBarAndroid, Button } from 'react-native';
import TodaysProjects from './TodaysProjects/TodaysProjectsComponent';
import SwipeCarousel from '../SwipeCarousel/SwipeCarouselComponent';
import RecentBlock from './RecentBlock/RecentBlockComponent';
import PortalsBlock from './PortalsBlock/PortalsBlockComponent';
import Menu from "../Menu/MenuComponent";
import _ from 'lodash';
import styles from './MainStyles';

class MainComponent extends React.Component {
  static navigationOptions = {
    header: null,
  };

  state = {
    loading: false,
  };

  async componentDidMount() {
    const loading = await this.props.GetAllProjects();
    this.setState({ loading });
  }

  startProject = id => {
    this.props.navigation.navigate('Project', { id });
  };

  removeProject = id => {
    this.props.DeleteProject(id);
  };

  getProgress = (totalScoring, stepsTotal) => totalScoring / stepsTotal;

  getProgressBarViaPlatform = (Color, totalScoring, stepsTotal) => {
    return Platform.select({
      ios: () => <ProgressViewIOS
        progress={this.getProgress(totalScoring, stepsTotal)}
        progressTintColor={Color}
        style={styles.progressBar}
      />,
      android: () => <ProgressBarAndroid
        progress={this.getProgress(totalScoring, stepsTotal)}
        styleAttr='Horizontal'
        indeterminate={false}
        color={Color}
        style={styles.progressBar}
      />,
    })()
  };

  render() {
    const { projects, scheduled, recentProjects, ...other } = this.props;

    const score = _.get(this.props.user, 'login.statistics.score', 0);
    return (
      <View style={styles.MainScreenWrap}>
        <ScrollView style={styles.MainScreen}>
          <SwipeCarousel
            component={TodaysProjects}
            data={scheduled}
            {...other}
          />
          <PortalsBlock
            projects={projects}
            getProgressBarViaPlatform={this.getProgressBarViaPlatform}
            callback={this.startProject}
            longPressCallback={this.removeProject}
            {...other}
          />
          <RecentBlock
            projects={recentProjects}
            getProgressBarViaPlatform={this.getProgressBarViaPlatform}
            callback={this.startProject}
            {...other}
          />
        </ScrollView>
        <Menu
          score={score}
          {...other}
        />
      </View>
    );
  }
}

export default MainComponent;