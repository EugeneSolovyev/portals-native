import React from 'react';
import { View, ScrollView, StyleSheet, Platform } from 'react-native';
import ProjectNav from '../ProjectNav/ProjectNavComponent';
import ProjectPreview from '../../ProjectPreview/ProjectPreviewComponent';
import styles from './PortalsBlockStyles';

class PortalsBlock extends React.Component {
  render() {
    const { projects, navigation } = this.props;
    return (
      <View style={styles.AvailablePortalsBlock}>
        <ProjectNav
          projects={projects}
          navigation={navigation}
        />
        <ScrollView horizontal>
          {projects.map((project, key) => (
            <ProjectPreview
              callback={this.props.callback}
              longPressCallback={this.props.longPressCallback}
              getProgressBarViaPlatform={this.props.getProgressBarViaPlatform}
              key={key}
              {...project}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

export default PortalsBlock;