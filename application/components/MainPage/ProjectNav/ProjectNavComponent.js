import React from 'react';
import { View, StyleSheet, Text, Alert, TouchableOpacity, Image } from 'react-native';
import styles from './ProjectNavStyles';

const GetBorderRandomValue = () => Math.floor(Math.random() * 40) + 20;

class ProjectNav extends React.Component {
  state = {
    borderTopLeftRadius: GetBorderRandomValue(),
    borderTopRightRadius: GetBorderRandomValue(),
    borderBottomRightRadius: GetBorderRandomValue(),
    borderBottomLeftRadius: GetBorderRandomValue(),
  };
  
  _addProject = () => {
    this.props.navigation.navigate('NewProject');
  };
  
  render() {
    const { projects } = this.props;
    
    return (
      <View style={styles.projectNav}>
        <TouchableOpacity
          style={{...styles.buttonAdd, ...this.state}}
          onPress={this._addProject}
        >
          <Image
            source={require('./img/add.png')}
            style={{width: 20, height: 20}}
          />
        </TouchableOpacity>
        <Text style={styles.portals}>My portals ({projects.length})</Text>
      </View>
    );
  }
}

export default ProjectNav;