import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  projectNav: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
  buttonAdd: {
    width: 40,
    height: 40,
    backgroundColor: '#3498db',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    borderRadius: 50,
  },
  portals: {
    fontSize: 25,
    fontWeight: '100',
  },
});
