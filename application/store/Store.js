import { compose, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Store from '../reducers/RootReducer';

import { ProjectStore } from 'portals-common-classes';
ProjectStore.bindStore(Store);

const middleware = [thunk];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(Store, composeEnhancers(applyMiddleware(...middleware)));
